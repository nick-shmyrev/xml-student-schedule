/*jshint esversion: 6 */

(function() {
  "use strict";

  const xmlhttp = new XMLHttpRequest();

  xmlhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      renderSchedule(this);
    }
  };

  xmlhttp.open('get', 'VALID_timetable-john_doe.xml', true);
  xmlhttp.send();
})();

function renderSchedule(xml) {
  "use strict";

  const xmlDoc = xml.responseXML;

  const student = xmlDoc.children[0].children[0];
  document.getElementById('name').textContent = student.attributes['student-name'].textContent;
  document.getElementById('student-id').textContent = student.attributes['id'].textContent;

  const schoolInfo = xmlDoc.children[0].children[1];
  document.getElementById('school').textContent = schoolInfo.querySelector('school-name') ? schoolInfo.querySelector('school-name').textContent : '';
  document.getElementById('program').textContent = schoolInfo.querySelector('program') ? schoolInfo.querySelector('program').textContent : '';
  document.getElementById('term').textContent = schoolInfo.querySelector('term') ? schoolInfo.querySelector('term').textContent : '';

  renderTable();

  const timetable = xmlDoc.children[0].children[2].children;

  // Loop over days
  for (let i = 0; i < timetable.length; i++) {

    const day = xmlDoc.children[0].children[2].children[i].children;

    // Loop over class hours, if there is data for hour print it to appropriate cell
    for (let j = 0; j < day.length; j++) {
      const courseHour = day[j].children[0].textContent;
      const courseCode = day[j].children[1].textContent;
      const courseName = day[j].children[2].textContent;
      const courseRoom = day[j].children[3].textContent;

      const cell = document.getElementById(`d${i + 1}-h${courseHour / 100}`);
      cell.innerHTML = `${courseCode}<br>${courseName}<br>${courseRoom}`;
    }
  }

}

function renderTable() {
  "use strict";
  const tbody = document.querySelector('table tbody');
  for (let row = 0; row < 13; row++) {

    let tr = document.createElement('tr');

    for (let col = 0; col < 6; col++) {


      if (col === 0) {
        // Create hours cells
        let th = document.createElement('th');
        th.textContent = `${row + 8}:00`;
        tr.appendChild(th);
      } else {
        // Create table cells
        let td = document.createElement('td');
        td.setAttribute('id', `d${col}-h${row + 8}`);
        tr.appendChild(td);
      }
    }
    tbody.appendChild(tr);
  }
}